var webpack = require('webpack')
var path = require('path')
var HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: ['./src/main.js'],
  output: {
    path: path.join(process.cwd(), 'build'),
    filename: 'bundle.js',
    publicPath: ''
  },
  resolve: {
    extensions: ['', '.js', '.vue', '.css']
  },
  module: {
    loaders: [
      {
        test: /\.js$/, loaders: ['babel'],
        exclude: [/node_modules/]
      },
      {
        test: /\.vue$/,
        loaders: ['vue']
      },
      {
        test: /\.hbs$/,
        loaders: ['handlebars']
      },
      {
      test: /\.css$/,
      loader: 'css'
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'url?limit=100&name=resources/images/[hash].[ext]',
        exclude: [/node_modules/]
      }
    ]
  },
  vue: {
    autoprefixer: false,
    loaders: {
      js: 'babel'
    },
    postcss:[
      require('postcss-cssnext')()
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
        filename: path.join(process.cwd(), 'build/index.html'),
        title:  'E10服务云',
        template: __dirname + '/index.hbs',
        inject: false
    })
  ],
  babel: {
    presets: ['es2015', 'stage-0'],
    plugins: ['transform-runtime']
  }
}
